local B = unpack(select(2, ...))

SlashCmdList['ABERON_PETGUID'] = function()
    local summonedPetGUID = C_PetJournal.GetSummonedPetGUID()
    if summonedPetGUID then
        B.print(summonedPetGUID)
    end
end
SLASH_ABERON_PETGUID1 = '/petguid'

SlashCmdList['ABERON_MOUNTID'] = function(spellID)
    if spellID == nil then return end
    local mountID = C_MountJournal.GetMountIDs()
	for _, mountID in pairs(mountID) do
        local mountName, mountSpellID = C_MountJournal.GetMountInfoByID(mountID)
        if mountSpellID == tonumber(spellID) then
            B.print(mountName..' '.. mountID)
        end
    end
end
SLASH_ABERON_MOUNTID1 = '/mountid'

SlashCmdList['ABERON_ZONEID'] = function()
    B.print(C_Map.GetBestMapForUnit('player'))
end
SLASH_ABERON_ZONEID1 = '/zoneid'
