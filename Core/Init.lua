local addonName, ns = ...
ns[1] = {} -- B, Basement
ns[2] = {} -- C, Config
ns[3] = {} -- L, Locales
ns[4] = {} -- DB, Database

_G[addonName] = ns
