local addonName, ns = ...
local _, _, _, DB = unpack(ns)

DB.version = GetAddOnMetadata(addonName, 'Version')
DB.addonName = addonName

DB.playerName = UnitName('player')
DB.playerGuid = UnitGUID('player')
DB.playerRealm = GetRealmName()
DB.playerFaction = UnitFactionGroup('player')
DB.playerRace = select(2, UnitRace('player'))
DB.playerClass = select(2, UnitClass('player'))

DB.greyColor = '9d9d9d'

local frame = CreateFrame('Frame')
frame:RegisterEvent('PLAYER_LOGIN')
frame:RegisterEvent('PLAYER_TALENT_UPDATE')
frame:SetScript('OnEvent', function()
    local tree = GetSpecialization()
	if not tree then return end
	DB.role = select(5, GetSpecializationInfo(tree))
end)
