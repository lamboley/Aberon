local _, _, L = unpack(select(2, ...))
if GetLocale() ~= 'frFR' then return end

L['Mailbox'] = 'Boîte aux lettres'
L['Trivial Quests'] = 'Quêtes de bas niveau'
L['Track Quest POIs'] = "Suivi des points d’intérêts de quêtes"
L['do not exist'] = "n'existe pas"
L[' is grey.'] = ' est gris.'
L[' is locked.'] = ' est verrouillé.'
L['Quest accepted'] = 'Quête acceptée'
L['completed.'] = 'terminée.'
L['Experience gained'] = 'Expérience gagnée'
L['Discovered'] = 'Découverte'
L['You are no longer rested.'] = 'êtes plus reposé(e).'
L['You feel rested.'] = 'Vous vous sentez reposé(e).'
L['You create'] = 'Vous créez'
L['Received'] = 'Reçu'
L["You can't call heirloom mount since you haven't choosen a faction."] = "Vous ne pouvez pas appeler la monture d'héritage tant que vous n'avez pas choisi de faction."
