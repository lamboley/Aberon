local _, _, L = unpack(select(2, ...))

L['Mailbox'] = 'Mailbox'
L['Trivial Quests'] = 'Trivial Quests'
L['Track Quest POIs'] = 'Track Quest POIs'
L['do not exist'] = 'do not exist'
L[' is grey.'] = ' is grey.'
L[' is locked.'] = ' is locked.'
L['Quest accepted'] = 'Quest accepted'
L['completed.'] = 'completed.'
L['Experience gained'] = 'Experience gained'
L['Discovered'] = 'Discovered'
L['You are no longer rested.'] = 'You are no longer rested.'
L['You feel rested.'] = 'You feel rested.'
L['You create'] = 'You create'
L['Received'] = 'Received'
L["You can't call heirloom mount since you haven't choosen a faction."] = "You can't call heirloom mount since you haven't choosen a faction."
