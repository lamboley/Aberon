## Interface: 80300
## Title: |cffff7d00Aberon|r
## Author: Aberon
## Version: 0.0.7
## Dependencies: Blizzard_TradeSkillUI

Core\Init.lua

Locales\enUS.lua
Locales\frFR.lua

Core\Database.lua
Core\Function.lua
Core\DevTools.lua

Config\Settings.lua

Modules\Misc\Core.lua
Modules\Misc\AutoBuy.lua
Modules\Misc\AutoRepair.lua
Modules\Misc\AutoSell.lua
Modules\Misc\Bindings.lua
Modules\Misc\CVars.lua
Modules\Misc\EnhanceLoot.lua
Modules\Misc\Mount.lua

Modules\Tooltip\Core.lua

Modules\Alerts\OutOfMana.lua
Modules\Alerts\Resurrection.lua
Modules\Alerts\ThanksResurrection.lua

Modules\Chat\Core.lua

Modules\Tradeskill\Core.lua

Modules\ObjectiveTracker\Core.lua

Modules\UnitFrames\Core.lua
