local B, C = unpack(select(2, ...))

local function spellLink(id)
    local link = GetSpellLink(id)
	if link then
		return link
	else
		B.PrintMessage('GetSpellLink for ['..id..'] failed.')
		return '?'
	end
end

C['misc'] = {
    ['enable'] = true,
    ['raidTarget'] = true,
    ['forceSelfRaidTarget'] = true,
    ['forceBattlePet'] = true,
    ['muteSounds'] = true,
    ['battlePet'] = 'BattlePet-0-00000C78EEA2', -- Chouettepygmée
    --['battlePet'] = 'BattlePet-0-00000D788B4D', -- Zéphyr Espiègle
    --['battlePet'] = 'BattlePet-0-00000DE0F169', -- Unborn Val'kyr
}

C['cvars'] = {
    ['enable'] = true
}

C['tooltip'] = {
    ['enable'] = true
}

C['tradeskill'] = {
    ['enable'] = true
}

C['mount'] = {
    ['enable'] = true,
    ['mountHeirloom'] = {
        ['Alliance'] = 679,
        ['Horde'] = 678
    }
}

C['alerts'] = {
    ['enable'] = true,
    ['resurrectAlert'] = true,
    ['outOfManaAlert'] = true,
    ['resurrectThanksAlert'] = true,
    ['spellResurrect'] = {
        -- Shaman
        [2008] = { ['mass'] = false, ['link'] = spellLink(2008) },
        [212048] = { ['mass'] = true, ['link'] = spellLink(212048) },
        -- Priest
        [2006] = { ['mass'] = false, ['link'] = spellLink(2006) },
        [212036] = { ['mass'] = true, ['link'] = spellLink(212036) },
        -- Monk
        [115178] = { ['mass'] = false, ['link'] = spellLink(115178) },
        [212051] = { ['mass'] = true, ['link'] = spellLink(212051) },
        -- Paladin
        [7328] = { ['mass'] = false, ['link'] = spellLink(7328) },
        [212056] = { ['mass'] = true, ['link'] = spellLink(212056) },
        -- Druid
        [20484] = { ['mass'] = false, ['link'] = spellLink(20484) },
        [50769] = { ['mass'] = false, ['link'] = spellLink(50769) },
        [212040] = { ['mass'] = true, ['link'] = spellLink(212040) },
        -- Death Knight
        [61999] = { ['mass'] = false, ['link'] = spellLink(61999) }
    },
}

C['enhanceLoot'] = {
    ['enable'] = true
}

C['autoRepair'] = {
    ['enable'] = true
}

C['autoSell'] = {
    ['enable'] = true,
    ['whitelist'] = {
        [5956] = true, -- Marteau de forgeron
        [2901] = true, -- Pioche de mineur
        [40892] = true, -- Pioche-marteau
        [6218] = true, -- Bâtonnet runique en cuivre
        [6219] = true, -- Trousse de joaillier
        [6256] = true, -- Canne à pêche
        [6365] = true, -- Canne à pêche solide
        [45858] = true, -- Canne à pêche porte-bonheur de Nat
        [20815] = true -- Clé plate
    }
}

C['autoBuy'] = {
    ['enable'] = true,
    ['list'] = {
        [20815] = 1,
        [4371] = 999,
        [6533] = 999,
        [4364] = 999,
        [4357] = 999,
        [4382] = 999,
        [4404] = 999
    }
}

C['chat'] = {
    ['enable'] = true
}

C['bindings'] = {
    ['enable'] = true
}

C['unitFrames'] = {
    ['enable'] = true
}

C['objectiveTracker'] = {
    ['enable'] = true,
    ['scale'] = 0.85
}
