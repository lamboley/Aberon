local _, C = unpack(select(2, ...))
if C['tooltip']['enable'] ~= true then return end

local frame = CreateFrame('Frame')
frame:RegisterEvent('PLAYER_REGEN_ENABLED')
frame:RegisterEvent('PLAYER_REGEN_DISABLED')
frame:SetScript('OnEvent', function(_, event)
    if event == 'PLAYER_REGEN_ENABLED' then
        GameTooltip:SetScript('OnShow',GameTooltip.Show)
    elseif event == 'PLAYER_REGEN_DISABLED' then
        GameTooltip:SetScript('OnShow',GameTooltip.Hide)
    end
end)

hooksecurefunc('GameTooltip_SetDefaultAnchor', function(tooltip)
    tooltip:SetOwner(UIParent, 'ANCHOR_BOTTOMRIGHT')
end)
