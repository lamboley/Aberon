local _, C = unpack(select(2, ...))
if C['chat']['enable'] ~= true then return end

ChatFrame1Tab:SetScript('OnShow', ChatFrame1Tab.Hide)
ChatFrame1Tab:Hide()

local frame = CreateFrame('Frame')
frame:RegisterEvent('UI_SCALE_CHANGED')
frame:RegisterEvent('PLAYER_ENTERING_WORLD')

frame:SetScript('OnEvent', function()
    ChatFrame1:SetClampedToScreen(true)
    ChatFrame1:SetClampRectInsets(-1, 0, 0, 0)
    ChatFrame1:ClearAllPoints()
    ChatFrame1:SetPoint('BOTTOMLEFT', nil, 'BOTTOMLEFT', 0, 3)
    ChatFrame1:SetUserPlaced(true)
    ChatFrame1:SetSize(500, 150)

    ChatFrame1EditBox:ClearAllPoints()
    ChatFrame1EditBox:SetPoint('BOTTOMLEFT', ChatFrame1, 'TOPLEFT', 0, 3)
    ChatFrame1EditBox:SetPoint('BOTTOMRIGHT', ChatFrame1, 'TOPRIGHT', 0, 3)

    if (ChatFrame2.isDocked or ChatFrame2:IsShown()) then
        FCF_Close(ChatFrame2)
    end
    ChatFrame2.isDocked = 1
end)

ChatFrame_AddMessageEventFilter('CHAT_MSG_SYSTEM', function(_, _, msg)
    if msg:find('Quête%sacceptée', 1) then return true end
end)

ChatFrame_AddMessageEventFilter('CHAT_MSG_SYSTEM', function(_, _, msg)
    if msg:find('Nouveau%sdonjon%sdéverrouillé', 1) then return true end
end)

ChatFrame_AddMessageEventFilter('CHAT_MSG_SYSTEM', function(_, _, msg)
    if msg:find('Félicitations', 1) then return true end
end)

ChatFrame_AddMessageEventFilter('CHAT_MSG_SYSTEM', function(_, _, msg)
    if msg:find('Expérience%sgagnée', 1) then return true end
end)

ChatFrame_AddMessageEventFilter('CHAT_MSG_SYSTEM', function(_, _, msg)
    if msg:find('terminée.') then return true end
end)

ChatFrame_AddMessageEventFilter('CHAT_MSG_SYSTEM', function(_, _, msg)
    if msg:find('Reçu', 1) then return true end
end)
