local _, C = unpack(select(2, ...))
if C['tradeskill']['enable'] ~= true then return end

TradeSkillFrame:SetHeight(51 * 16 + 96)
TradeSkillFrame.RecipeInset:SetHeight(51 * 16 + 10)
TradeSkillFrame.DetailsInset:SetHeight(51 * 16 - 10)
TradeSkillFrame.DetailsFrame:SetHeight(51 * 16 - 15)
TradeSkillFrame.DetailsFrame.Background:SetHeight(51 * 16 - 17)
TradeSkillFrame.LinkToButton:SetPoint('BOTTOMRIGHT', TradeSkillFrame, 'TOPRIGHT', -10, -81)
TradeSkillFrame.FilterButton:SetPoint('TOPRIGHT', TradeSkillFrame, 'TOPRIGHT', -12, -31)
TradeSkillFrame.FilterButton:SetHeight(17)
TradeSkillFrame.RankFrame:SetPoint('TOP', TradeSkillFrame, 'TOP', -17, -33)
TradeSkillFrame.RankFrame:SetWidth(500)
TradeSkillFrame.SearchBox:SetPoint('TOPLEFT', TradeSkillFrame, 'TOPLEFT', 163, -60)
TradeSkillFrame.SearchBox:SetWidth(97)

if TradeSkillFrame.RecipeList.FilterBar:IsVisible() then
    TradeSkillFrame.RecipeList:SetHeight(51 * 16 - 11)
else
    TradeSkillFrame.RecipeList:SetHeight(51 * 16 + 5)
end

if #TradeSkillFrame.RecipeList.buttons < floor(51, 0.5) + 2 then
    local range = TradeSkillFrame.RecipeList.scrollBar:GetValue()
    HybridScrollFrame_CreateButtons(TradeSkillFrame.RecipeList, 'TradeSkillRowButtonTemplate', 0, 0)
    TradeSkillFrame.RecipeList.scrollBar:SetValue(range)
end
TradeSkillFrame.RecipeList:Refresh()

hooksecurefunc(TradeSkillFrame.RecipeList, 'UpdateFilterBar', function(self)
	if self.FilterBar:IsVisible() then
		self:SetHeight(51 * 16 - 11)
	else
		self:SetHeight(51 * 16 + 5)
	end
end)
