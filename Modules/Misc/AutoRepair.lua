local _, C = unpack(select(2, ...))
if C['autoRepair']['enable'] ~= true then return end

local frame = CreateFrame('Frame')
frame:RegisterEvent('MERCHANT_SHOW')
frame:SetScript('OnEvent', function()
    if CanMerchantRepair() then
        if select(1, GetGuildInfo('player')) then RepairAllItems(true) end
        RepairAllItems()
    end
end)
