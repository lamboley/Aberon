local _, C = unpack(select(2, ...))
if C['bindings']['enable'] ~= true then return end

local function CreateBindingFrame(name, key, action)
    local btn = CreateFrame('BUTTON', 'ABERONBINDINGFRAME_' .. name)
    SetBindingClick(key, btn:GetName())
    btn:SetScript('OnClick', action)
end

local function CreateBindingMacro(name, texture, action, key)
    local macroIconTexture = texture or 'INV_MISC_QUESTIONMARK'
    if GetMacroIndexByName(name) == 0 then
        CreateMacro(name, macroIconTexture, action, nil)
    else
        EditMacro(name, name, macroIconTexture, action, 1, nil)
    end
    SetBindingMacro(key, name)
end

local frame = CreateFrame('Frame')
frame:RegisterEvent('PLAYER_LOGIN')
frame:SetScript('OnEvent', function()
    CreateBindingMacro('HEARTHSTONE_TOY', nil, '/use Pierre de foyer du voyageur éternel', 'F4')
    CreateBindingFrame('RELOAD', 'F12', function() ReloadUI() end)
    CreateBindingFrame('TOGGLE_MINIMAP', 'F3', function()
        if MinimapCluster:IsShown() then
            MinimapCluster:Hide()
        else
            MinimapCluster:Show()
        end
    end)
end)
