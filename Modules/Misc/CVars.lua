local _, C = unpack(select(2, ...))
if C['cvars']['enable'] ~= true then return end

SetCVar('cameraDistanceMaxZoomFactor', 2.6)
SetCVar('nameplateMaxDistance', 120)
SetCVar('floatingCombatTextCombatDamage', 0)
SetCVar('removeChatDelay', 0)
SetCVar('wholeChatWindowClickable', 1)
SetCVar('chatStyle', 'classic')
SetCVar('useCompactPartyFrames', 1)
SetCVar('nameplateShowSelf', 0)
SetCVar('nameplateShowEnemyMinions', 0)
SetCVar('nameplateShowAll', 1)
