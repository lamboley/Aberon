local _, C = unpack(select(2, ...))
if C['autoSell']['enable'] ~= true then return end

local frame = CreateFrame('Frame')
frame:RegisterEvent('MERCHANT_SHOW')
frame:SetScript('OnEvent', function()
    for sacID = 0, NUM_BAG_SLOTS do
        for slotID = 1, GetContainerNumSlots(sacID) do
            local itemID = GetContainerItemID(sacID, slotID)
            if itemID and not C['autoSell']['whitelist'][itemID] then
                local _, _, itemRarity, _, _, _, _, _, _, _, _, itemClassID = GetItemInfo(itemID)
                if itemRarity == 0 or (itemRarity == 1 and (itemClassID == 2 or itemClassID == 4)) then
                    UseContainerItem(sacID, slotID)
                end
            end
        end
    end
end)
