local _, C = unpack(select(2, ...))
if C['autoBuy']['enable'] ~= true then return end

local frame = CreateFrame('Frame')
frame:RegisterEvent('MERCHANT_SHOW')
frame:SetScript('OnEvent', function()
    for index = 1, GetMerchantNumItems() do
        local itemID = GetMerchantItemID(index)
        if itemID then
            local countToKeep = C['autoBuy']['list'][itemID]
            if countToKeep and countToKeep == 999 then
                local _, _, _, quantity, numAvailable = GetMerchantItemInfo(index)
                for i = 1, numAvailable, quantity do
                    BuyMerchantItem(index, quantity)
                end
            end
        end
    end
end)
