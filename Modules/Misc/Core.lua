local _, C, _, DB = unpack(select(2, ...))
if C['misc']['enable'] ~= true then return end

--------------------------------------------------------------------------------
--	Resize or hide some frame
--------------------------------------------------------------------------------
local frame = CreateFrame('Frame')
frame:RegisterEvent('PLAYER_ENTERING_WORLD')
frame:SetScript('OnEvent', function()
    MiniMapWorldMapButton:Hide()
    GameTimeFrame:Hide()
    MinimapZoomIn:Hide()
    ChatFrameMenuButton:Hide()
    ChatFrameMenuButton:SetScript('OnEvent', nil)
    MinimapZoomOut:Hide()
    PlayerFrame:SetScript('OnEvent', nil)
    PlayerFrame:Hide()
    TargetFrame:SetScript('OnEvent', nil)
    TargetFrame:Hide()
    MinimapNorthTag:SetTexture(nil)
    VehicleSeatIndicator:SetScale(0.85)
end)

--------------------------------------------------------------------------------
--	Sort bag when opened
--------------------------------------------------------------------------------
hooksecurefunc('OpenBackpack', function()
    BagItemAutoSortButton:UnregisterAllEvents()
    BagItemAutoSortButton:Hide()
    if not MerchantFrame:IsVisible() then
        SortBags()
    end
end)

--------------------------------------------------------------------------------
--	Disable by default useless addon
--------------------------------------------------------------------------------
DisableAddOn('Kui_Media')
DisableAddOn('WeakAurasArchive')
DisableAddOn('WeakAurasCompanion')
DisableAddOn('WeakAurasModelPaths')
DisableAddOn('WeakAurasTemplates')
DisableAddOn('Details_EncounterDetails')
DisableAddOn('Details_RaidCheck')
DisableAddOn('Details_Streamer')
DisableAddOn('Details_TinyThreat')
DisableAddOn('Details_Vanguard')
DisableAddOn('OmniCC_Config')

--------------------------------------------------------------------------------
--	Hide mini list of AllTheThings
--------------------------------------------------------------------------------
local frame = CreateFrame('Frame')
frame:RegisterEvent('PLAYER_ENTERING_WORLD')
frame:SetScript('OnEvent', function(_, event)
    if event == 'PLAYER_ENTERING_WORLD' then
        if IsAddOnLoaded('AllTheThings') then
            frame:RegisterEvent('PLAYER_REGEN_ENABLED')
            frame:RegisterEvent('PLAYER_REGEN_DISABLED')
        end
    elseif event == 'PLAYER_REGEN_ENABLED' then
        local self = AllTheThings:GetWindow("CurrentInstance")
        if not self:IsVisible() then
            AllTheThings.OpenMiniListForCurrentZone()
        end
    elseif event == 'PLAYER_REGEN_DISABLED' then
        local self = AllTheThings:GetWindow("CurrentInstance")
        if self:IsVisible() then
            self:Hide()
        end
    end
end)

--------------------------------------------------------------------------------
--	Mute some annoying sound
--------------------------------------------------------------------------------
if C['misc']['muteSounds'] == true then
    local blacklistSound = {
        569854, -- sound/vehicles/motorcyclevehicle/motorcyclevehiclewalkrun.ogg
        569858, -- sound/vehicles/motorcyclevehicle/motorcyclevehicleattackthrown.ogg
        569859, -- sound/vehicles/motorcyclevehicle/motorcyclevehiclestand.ogg
        569861, -- sound/vehicles/motorcyclevehicle/motorcyclevehicleloadthrown.ogg
        569856, -- sound/vehicles/motorcyclevehicle/motorcyclevehiclejumpstart1.ogg
        569862, -- sound/vehicles/motorcyclevehicle/motorcyclevehiclejumpstart2.ogg
        569860, -- sound/vehicles/motorcyclevehicle/motorcyclevehiclejumpstart3.ogg
        569863, -- sound/vehicles/motorcyclevehicle/motorcyclevehiclejumpend1.ogg
        569855, -- sound/vehicles/motorcyclevehicle/motorcyclevehiclejumpend3.ogg
        569857, -- sound/vehicles/motorcyclevehicle/motorcyclevehiclejumpend2.ogg
        598748, -- sound/vehicles/vehicle_ground_gearshift_1.ogg
        598736, -- sound/vehicles/vehicle_ground_gearshift_2.ogg
        569852, -- sound/vehicles/vehicle_ground_gearshift_3.ogg
        598745, -- sound/vehicles/vehicle_ground_gearshift_4.ogg
        569845, -- sound/vehicles/vehicle_ground_gearshift_5.ogg
        1663845, -- sound/creature/manaray/mon_manaray_chuff_01.ogg
        1663846, -- sound/creature/manaray/mon_manaray_chuff_02.ogg
        1663826, -- sound/creature/manaray/mon_manaray_chuff_03.ogg
        1663827, -- sound/creature/manaray/mon_manaray_chuff_04.ogg
        1663828, -- sound/creature/manaray/mon_manaray_chuff_05.ogg
        1663829, -- sound/creature/manaray/mon_manaray_chuff_06.ogg
        1663830, -- sound/creature/manaray/mon_manaray_chuff_07.ogg
        1663831, -- sound/creature/manaray/mon_manaray_chuff_08.ogg
        1663835, -- sound/creature/manaray/mon_manaray_summon_01.ogg
        1663836, -- sound/creature/manaray/mon_manaray_summon_02.ogg
        1323566, -- sound/creature/druidcat/mon_dr_catform_attack01.ogg
        1323567, -- sound/creature/druidcat/mon_dr_catform_attack02.ogg
        1323568, -- sound/creature/druidcat/mon_dr_catform_attack03.ogg
        1323569, -- sound/creature/druidcat/mon_dr_catform_attack04.ogg
        1323570, -- sound/creature/druidcat/mon_dr_catform_attack05.ogg
        1323571, -- sound/creature/druidcat/mon_dr_catform_attack06.ogg
        1323572, -- sound/creature/druidcat/mon_dr_catform_attack07.ogg
        1323573, -- sound/creature/druidcat/mon_dr_catform_attack08.ogg
        1323574, -- sound/creature/druidcat/mon_dr_catform_spellattack01.ogg
        1323575, -- sound/creature/druidcat/mon_dr_catform_spellattack02.ogg
        1323576, -- sound/creature/druidcat/mon_dr_catform_spellattack03.ogg
        1323577, -- sound/creature/druidcat/mon_dr_catform_spellattack04.ogg
        1323578, -- sound/creature/druidcat/mon_dr_catform_spellattack05.ogg
        1324558, -- sound/creature/druidcat/mon_dr_catform_wound01.ogg
        1324559, -- sound/creature/druidcat/mon_dr_catform_wound02.ogg
        1324560, -- sound/creature/druidcat/mon_dr_catform_wound03.ogg
        1324561, -- sound/creature/druidcat/mon_dr_catform_wound04.ogg
        1324562, -- sound/creature/druidcat/mon_dr_catform_wound05.ogg
        1324563, -- sound/creature/druidcat/mon_dr_catform_wound06.ogg
        1324564, -- sound/creature/druidcat/mon_dr_catform_wound07.ogg
        1324565, -- sound/creature/druidcat/mon_dr_catform_wound08.ogg
        1324566, -- sound/creature/druidcat/mon_dr_catform_woundcrit01.ogg
        1324567, -- sound/creature/druidcat/mon_dr_catform_woundcrit02.ogg
        1324568, -- sound/creature/druidcat/mon_dr_catform_woundcrit03.ogg
        1324569, -- sound/creature/druidcat/mon_dr_catform_woundcrit04.ogg
        1324570, -- sound/creature/druidcat/mon_dr_catform_woundcrit05.ogg
        600278, -- sound/creature/goblintrike/veh_goblintrike_turn_02.ogg
        600281, -- sound/creature/goblintrike/veh_goblintrike_idle_loop_01.ogg
        600290, -- sound/creature/goblintrike/veh_goblintrike_turn_01.ogg
        600293 -- sound/creature/goblintrike/veh_goblintrike_drive_loop_01.ogg
    }

    for _, s in pairs(blacklistSound) do
        MuteSoundFile(s)
    end
end

--------------------------------------------------------------------------------
--	Force raid target on myself when I am a healer
--------------------------------------------------------------------------------
if C['misc']['raidTarget'] == true then
    local frame = CreateFrame('Frame')
    frame:RegisterEvent('PLAYER_ENTERING_WORLD')
    frame:RegisterEvent('GROUP_LEFT')
    if C['misc']['forceSelfRaidTarget'] then frame:RegisterEvent('RAID_TARGET_UPDATE') end
    frame:SetScript('OnEvent', function()
        local instanceType = select(2, IsInInstance())
        if instanceType and (instanceType == 'party' or instanceType == 'raid' or instanceType == 'scenario') then
            if DB.role == 'HEALER' then
                if GetRaidTargetIndex('player') == nil then
                    SetRaidTarget('player', 4)
                end
            end
        else
            SetRaidTarget('player', 0)
        end
    end)
end

--------------------------------------------------------------------------------
-- Allways keep my favorite battle pet active
-- Dismiss battle pet when we are in stealth
--------------------------------------------------------------------------------
if C['misc']['forceBattlePet'] then
    local frame = CreateFrame('Frame')
    frame:RegisterEvent('PLAYER_ENTERING_WORLD')
    frame:RegisterEvent('COMPANION_UPDATE')
    frame:RegisterEvent('ZONE_CHANGED')
    frame:RegisterEvent('ZONE_CHANGED_INDOORS')
    frame:RegisterEvent('ZONE_CHANGED_NEW_AREA')
    frame:RegisterEvent('UPDATE_STEALTH')
    frame:SetScript('OnEvent', function(_, event)
        if event == 'UPDATE_STEALTH' then
            if InCombatLockdown('player') or not IsStealthed() then return end
            if C_PetJournal.GetSummonedPetGUID() ~= nil then
                C_PetJournal.SummonPetByGUID(C_PetJournal.GetSummonedPetGUID())
            end
        else
            if InCombatLockdown('player') or IsStealthed() or select(2, IsInInstance()) == 'pvp' then return end
            if C_PetJournal.GetSummonedPetGUID() ~= C['misc']['battlePet'] then
                C_PetJournal.SummonPetByGUID(C['misc']['battlePet'])
            end
        end
    end)
end
