local B, C, L, DB = unpack(select(2, ...))
if C['mount']['enable'] ~= true then return end

local mapCantFly = {
    [672] = true,
    [1339] = true,
    [112] = true,
    [206] = true,
    [423] = true,
    [1576] = true,
    [126] = true,
    [417] = true,
    [277] = true, -- Uldum dungeon
    [1366] = true,
    [595] = true, -- Quai de fer
    [130] = true, -- Stratholme dungeon
    [1165] = true, -- BFA Vol
    [863] = true, -- BFA Vol
    [275] = true
}

function M(groundID, flyingID)
    if CanExitVehicle() then
        VehicleExit()
    elseif not C_MountJournal.GetNumMounts() or IsMounted() then
        Dismount()
    elseif mapCantFly[C_Map.GetBestMapForUnit('player')] then
        C_MountJournal.SummonByID(tonumber(groundID))
    elseif IsSpellKnown(34090) ~= true and IsSpellKnown(34091) ~= true and IsSpellKnown(90265) ~= true then
        if IsSpellKnown(33388) ~= true and IsSpellKnown(33391) ~= true then
            if C_Map.GetBestMapForUnit('player') == 378 then
                B.print(L["You can't call heirloom mount since you haven't choosen a faction."])
            else
                C_MountJournal.SummonByID(C['mount']['mountHeirloom'][DB.playerFaction])
            end
        else
            C_MountJournal.SummonByID(tonumber(groundID))
        end
    elseif IsUsableSpell(93326) == true or IsFlyableArea() then
        C_MountJournal.SummonByID(tonumber(flyingID))
    else
        C_MountJournal.SummonByID(tonumber(groundID))
    end
end
