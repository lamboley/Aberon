local _, C = unpack(select(2, ...))
if C['enhanceLoot']['enable'] ~= true then return end

LootFrame:SetAlpha(0)

local frame = CreateFrame('Frame')
frame:RegisterEvent('LOOT_OPENED')
frame:SetScript('OnEvent', function()
    for i = 1, GetNumLootItems() do
        LootSlot(i)
    end
end)
