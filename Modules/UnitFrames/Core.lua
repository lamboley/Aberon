local _, C  = unpack(select(2, ...))
if C['unitFrames']['enable'] ~= true then return end

for i = 1, 4 do
    local BossFrame = _G['Boss'..i..'TargetFrame']
    if BossFrame then
        BossFrame:Hide()
        BossFrame:UnregisterAllEvents()
    end
end
