local _, C = unpack(select(2, ...))
if C['alerts']['outOfManaAlert'] ~= true then return end

local isLow, isOOM = false, false

local frame = CreateFrame('Frame')
frame:RegisterEvent('PLAYER_ENTERING_WORLD')
frame:SetScript('OnEvent', function(_, event, ...)
    if event == 'PLAYER_ENTERING_WORLD' then
        if select(2, IsInInstance()) == 'party' then
            frame:RegisterEvent('UNIT_POWER_UPDATE')
        else
            frame:UnregisterEvent('UNIT_POWER_UPDATE')
        end
    else
        local unitTarget, powerType = ...
        if unitTarget == 'player' and powerType == 'MANA' then
            local manaPercent = math.ceil(UnitPower(unitTarget, 0)*100/UnitPowerMax(unitTarget, 0))
            if not isLow and manaPercent < 31 then
                SendChatMessage('Mana: '..manaPercent..'%')
                isLow = 30
            elseif not isOOM and manaPercent < 8 then
                SendChatMessage('Out Of Mana: '..manaPercent..'%')
                isOOM = 8
            elseif isLow == 30 and manaPercent > 60 then
                isLow = false
            elseif isOOM == 8 and manaPercent > 20 then
                isOOM = false
            end
        end
    end
end)
