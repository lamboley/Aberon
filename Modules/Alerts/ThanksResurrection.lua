local _, C, _, DB = unpack(select(2, ...))
if C['alerts']['resurrectThanksAlert'] ~= true then return end

local frame = CreateFrame('Frame')
frame:RegisterEvent('PLAYER_ENTERING_WORLD')
frame:RegisterEvent('PLAYER_DEAD')
frame:RegisterEvent('PLAYER_ALIVE')
frame:RegisterEvent('PLAYER_UNGHOST')
frame:SetScript('OnEvent', function(_, event)
    if event == 'PLAYER_ENTERING_WORLD' or event == 'GROUP_LEFT' or event == 'GROUP_JOINED' then
        if UnitIsDead('player') then
            frame:RegisterEvent('COMBAT_LOG_EVENT_UNFILTERED')
        else
            frame:UnregisterEvent('COMBAT_LOG_EVENT_UNFILTERED')
        end
    else
        local _, subEvent, _, _, sourceName, _, _, _, destName, _, _, spellID = CombatLogGetCurrentEventInfo()
        if subEvent == 'SPELL_CAST_SUCCESS' then
            if sourceName then
                spellID = C['alerts']['spellResurrect'][spellID]
                if spellID then
                    if destName == DB.playerName or spellID['mass'] then
                        SendChatMessage('Thanks for the resurrect.', 'WHISPER', 'Common', sourceName)
                    end
                end
            end
        end
    end
end)
