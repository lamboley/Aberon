local _, C, _, DB = unpack(select(2, ...))
if C['alerts']['resurrectAlert'] ~= true then return end

local castingHeal = false

local frame = CreateFrame('Frame')
frame:RegisterEvent('PLAYER_ENTERING_WORLD')
frame:RegisterEvent('GROUP_LEFT')
frame:RegisterEvent('GROUP_JOINED')
frame:SetScript('OnEvent', function(_, event)
    if event == 'PLAYER_ENTERING_WORLD' or event == 'GROUP_LEFT' or event == 'GROUP_JOINED' then
        if IsInGroup() then
            frame:RegisterEvent('COMBAT_LOG_EVENT_UNFILTERED')
        else
            frame:UnregisterEvent('COMBAT_LOG_EVENT_UNFILTERED')
        end
    else
        local _, subEvent, _, _, sourceName, _, _, _, destName, _, _, spellID = CombatLogGetCurrentEventInfo()
        if sourceName and sourceName == DB.playerName then
            spellID = C['alerts']['spellResurrect'][spellID]
            if spellID then
                if spellID['mass'] then
                    destName = 'every dead players'
                else
                    destName = GetUnitName('target')
                end
                if subEvent == 'SPELL_CAST_START' then
                    SendChatMessage('Cast of ' .. spellID['link'] .. ' on ' .. destName ..' started ...')
                    castingHeal = true
                elseif subEvent == 'SPELL_CAST_FAILED' then
                    if castingHeal then
                        SendChatMessage('Cast of ' .. spellID['link'] .. ' on ' .. destName .. ' failed.')
                        castingHeal = false
                    end
                elseif subEvent == 'SPELL_CAST_SUCCESS' then
                    SendChatMessage('Cast of ' .. spellID['link'] .. ' on ' .. destName .. ' succeeded.')
                    castingHeal = false
                end
            end
        end
    end
end)
